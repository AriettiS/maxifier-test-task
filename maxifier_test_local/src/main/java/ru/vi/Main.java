package ru.vi;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.log4j.Logger;
import ru.vi.executor.RemoteServiceRequestExecutor;
import ru.vi.modules.AppInjector;

public class Main {
    private final static Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException {
        Injector injector = Guice.createInjector(new AppInjector());
        RemoteServiceRequestExecutor remoteServiceRequestExecutor = injector.getInstance(RemoteServiceRequestExecutor.class);
        remoteServiceRequestExecutor.execute();
    }
}
