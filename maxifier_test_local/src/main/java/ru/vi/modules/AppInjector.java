package ru.vi.modules;

import com.google.inject.AbstractModule;
import ru.vi.executor.BestSequenceFinder;
import ru.vi.executor.MinDelayBestSequenceFinder;
import ru.vi.http.IdGeneratorServiceHttpClient;
import ru.vi.http.IdGeneratorServiceHttpClientImpl;
import ru.vi.service.IdGenerator;
import ru.vi.service.IdGeneratorImpl;

public class AppInjector extends AbstractModule {
    @Override
    protected void configure() {
        bind(IdGeneratorServiceHttpClient.class).to(IdGeneratorServiceHttpClientImpl.class);
        bind(IdGenerator.class).to(IdGeneratorImpl.class);
        bind(BestSequenceFinder.class).to(MinDelayBestSequenceFinder.class);
    }

}