package ru.vi.service;

import java.util.concurrent.TimeoutException;

public interface IdGenerator {
    /**
     * Get next unique ID.
     * @param sequence name of sequence
     * @return next ID
     * @throws TimeoutException if timed out
     */
    long getNextId(String sequence) throws TimeoutException;
}
