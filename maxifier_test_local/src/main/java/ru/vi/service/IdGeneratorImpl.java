package ru.vi.service;

import com.google.inject.Singleton;
import org.apache.log4j.Logger;
import ru.vi.executor.RemoteServiceRequestExecutor;

import javax.inject.Inject;
import java.time.Instant;

@Singleton
public class IdGeneratorImpl implements IdGenerator {
    private final static Logger LOG = Logger.getLogger(IdGeneratorImpl.class);

    private RemoteServiceRequestExecutor remoteServiceRequestExecutor;

    @Inject
    public void setService(RemoteServiceRequestExecutor remoteServiceRequestExecutor){
        this.remoteServiceRequestExecutor = remoteServiceRequestExecutor;
    }

    @Override
    public long getNextId(String sequence) {
        try {
            long startTime = Instant.now().toEpochMilli();

            long result = remoteServiceRequestExecutor.demandId(sequence);

            long finishTime = Instant.now().toEpochMilli();
            LOG.trace("Result for " + sequence + ": " + result + ", thread: " + Thread.currentThread().getId()
                    + ", time (ms): " + (finishTime - startTime));

            return result;
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
