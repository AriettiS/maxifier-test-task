package ru.vi.http;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class HttpResponseAnalyzer {
    private final HttpResponse response;

    public HttpResponseAnalyzer(HttpResponse response) {
        this.response = response;
    }

    public int getCode() {
        return response.getStatusLine().getStatusCode();
    }

    public String getResult() {
        try {
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
