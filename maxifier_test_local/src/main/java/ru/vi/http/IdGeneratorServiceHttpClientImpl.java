package ru.vi.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.inject.Singleton;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Singleton
public class IdGeneratorServiceHttpClientImpl implements IdGeneratorServiceHttpClient {
    private static final String URI_BASE = "http://localhost:9000/getid/";
    private static final int CONNECTION_TIMEOUT_MS = 2000;

    @Override
    public RemoteRequestResult getNextId(String sequence, int count) {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

            URI uri = new URI(URI_BASE + sequence + "/" + count);
            HttpGet httpGet = new HttpGet(uri);

            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
                    .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                    .setSocketTimeout(CONNECTION_TIMEOUT_MS)
                    .build();
            httpGet.setConfig(requestConfig);

            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpResponseAnalyzer httpResponseAnalyzer = new HttpResponseAnalyzer(httpResponse);

            return new RemoteRequestResult(httpResponseAnalyzer.getCode(), httpResponseAnalyzer.getResult());

        } catch (IOException | URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }
}
