package ru.vi.http;

public interface IdGeneratorServiceHttpClient {
    RemoteRequestResult getNextId(String sequence, int count);
}
