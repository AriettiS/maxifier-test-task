package ru.vi.http;

import org.apache.http.HttpStatus;

public class RemoteRequestResult {
    private final int code;
    private final String result;

    public RemoteRequestResult(int code, String result) {
        this.code = code;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public String getResult() {
        return result;
    }

    public boolean isSuccessful() {
        return code == HttpStatus.SC_OK;
    }

    public long getResultId() {
        return Long.parseLong(result);
    }

    @Override
    public String toString() {
        return "RemoteRequestResult{" +
                "code=" + code +
                ", result='" + result + '\'' +
                '}';
    }
}
