package ru.vi.executor;

import org.apache.log4j.Logger;
import ru.vi.http.IdGeneratorServiceHttpClient;
import ru.vi.http.RemoteRequestResult;
import ru.vi.model.RequestQueueEntry;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;

@Singleton
public class RemoteServiceRequestExecutor {
    private final static Logger LOG = Logger.getLogger(RemoteServiceRequestExecutor.class);

    private final static long REMOTE_SERVICE_EXECUTION_DELAY_MS = 1000L;
    private final static int DEFAULT_NUMBER_OF_REQUEST_PER_MINUTE = 1000;

    private IdGeneratorServiceHttpClient idGeneratorServiceHttpClient;
    private BestSequenceFinder bestRequestQueueFinder;

    private Map<String, RequestQueueEntry> sequenceToRequestEntry = new ConcurrentHashMap<>();

    @Inject
    public void setIdGeneratorServiceHttpClient(IdGeneratorServiceHttpClient IdGeneratorServiceHttpClient) {
        this.idGeneratorServiceHttpClient = IdGeneratorServiceHttpClient;
    }

    @Inject
    public void setBestSequenceFinder(BestSequenceFinder bestRequestQueueFinder) {
        this.bestRequestQueueFinder = bestRequestQueueFinder;
    }

    public long demandId(String sequence) throws InterruptedException {
        sequenceToRequestEntry.putIfAbsent(sequence, new RequestQueueEntry());
        RequestQueueEntry requestQueueEntry = sequenceToRequestEntry.get(sequence);
        return requestQueueEntry.demandId();
    }

    /**
     * Start scheduled requests to the remote server
     */
    public void execute() {
        ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();
        execService.scheduleAtFixedRate(()-> {
                ExecutorService executor = Executors.newCachedThreadPool();
                executor.submit(()-> {
                    RequestQueueEntry biggestQueueEntry = null;
                    try {
                        Optional<String> bestSequenceOptional = bestRequestQueueFinder.find(sequenceToRequestEntry);
                        if (!bestSequenceOptional.isPresent()) {
                            return;
                        }

                        String bestSequence = bestSequenceOptional.get();
                        biggestQueueEntry = sequenceToRequestEntry.get(bestSequence);
                        LOG.debug(biggestQueueEntry.getBufferFreeSize());
                        int countForRemoteService = Math.min(DEFAULT_NUMBER_OF_REQUEST_PER_MINUTE,
                                biggestQueueEntry.getBufferFreeSize());

                        if (countForRemoteService == 0) {
                            return;
                        }

                        biggestQueueEntry.setProcessing(true);
                        RemoteRequestResult remoteRequestResult = idGeneratorServiceHttpClient
                                .getNextId(bestSequence, countForRemoteService);

                        if (!remoteRequestResult.isSuccessful()) {
                            LOG.error("HTTP request failed: " + remoteRequestResult);
                            return;
                        }

                        biggestQueueEntry.handleRequests(countForRemoteService, remoteRequestResult.getResultId());
                    } catch (Exception e) {
                        LOG.error("Error while getting id: ", e);
                    } finally {
                        if (biggestQueueEntry != null) {
                            biggestQueueEntry.setProcessing(false);
                        }
                    }

                });
        }, 0, REMOTE_SERVICE_EXECUTION_DELAY_MS, TimeUnit.MILLISECONDS);
    }
}
