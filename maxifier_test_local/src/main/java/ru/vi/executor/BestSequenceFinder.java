package ru.vi.executor;

import ru.vi.model.RequestQueueEntry;

import java.util.Map;
import java.util.Optional;

public interface BestSequenceFinder {
    /**
     * Find best sequence to process
     * @param map sequence to RequestQueueEntry
     * @return best sequence to process
     */
    Optional<String> find(Map<String, RequestQueueEntry> map);
}
