package ru.vi.executor;

import com.google.inject.Singleton;
import org.apache.log4j.Logger;
import ru.vi.model.RequestQueueEntry;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class MinDelayBestSequenceFinder implements BestSequenceFinder {
    private final static Logger LOG = Logger.getLogger(MinDelayBestSequenceFinder.class);

    /**
     * Return sequence with maximum waiting time of queue
     */
    @Override
    public Optional<String> find(Map<String, RequestQueueEntry> map) {
        long currentTimeMilli = Instant.now().toEpochMilli();

        Map<String, Long> sequenceToSumOfWaitTime = new HashMap<>();
        Set<Map.Entry<String, RequestQueueEntry>> notProcessingEntries = map.entrySet()
                .stream()
                .filter(stringRequestQueueEntryEntry -> !stringRequestQueueEntryEntry.getValue().isProcessing()).collect(Collectors.toSet());
        for (Map.Entry<String, RequestQueueEntry> entry : notProcessingEntries) {
            long sum = 0;
            String sequence = entry.getKey();

            for (long waitTime : entry.getValue().getCollectionOfRegisterTime()) {
                sum += currentTimeMilli - waitTime;
            }
            if (sum != 0) {
                sequenceToSumOfWaitTime.put(sequence, sum);
            }
        }

        return sequenceToSumOfWaitTime.entrySet()
                .stream()
                .max((o1, o2) -> o1.getValue().compareTo(o2.getValue())).map(Map.Entry::getKey);

    }
}
