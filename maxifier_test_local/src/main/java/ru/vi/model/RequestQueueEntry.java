package ru.vi.model;

import org.apache.log4j.Logger;

import java.time.Instant;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

public class RequestQueueEntry {
    private final static Logger LOG = Logger.getLogger(RequestQueueEntry.class);

    public static final int QUEUE_CAPACITY = 1000;
    public final static int WAIT_RESULT_TIMEOUT_SECONDS = 2000;

    private final BlockingQueue<Long> idsQueue = new ArrayBlockingQueue<>(QUEUE_CAPACITY);
    private final ConcurrentLinkedQueue<Long> registerTime = new ConcurrentLinkedQueue<>();

    private volatile boolean isProcessing;

    public Long demandId() throws InterruptedException {
        this.registerTime.add(Instant.now().toEpochMilli());

        return idsQueue.poll(WAIT_RESULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    public int getBufferFreeSize() {
        return QUEUE_CAPACITY - idsQueue.size();
    }

    public void handleRequests(int count, long firstId) {
        for (int i = 0; i < count; i++) {
            this.registerTime.poll();
            idsQueue.add(firstId + i);
        }
    }

    public Collection<Long> getCollectionOfRegisterTime()
    {
        return registerTime;
    }

    /**
     * @return true if new ids have been already demanded or false otherwise
     */
    public boolean isProcessing() {
        return isProcessing;
    }

    /**
     * set flag whether new ids have been already demanded or not
     */
    public void setProcessing(boolean processing) {
        isProcessing = processing;
    }
}
