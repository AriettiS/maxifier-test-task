package ru.vi.http;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RemoteRequestResultTest {

    public static final int SUCCESS_HTTP_CODE = 200;
    public static final int FAIL_HTTP_CODE = 403;
    public static final String COMMON_RESULT_ID_STRING = "11111";
    public static final long COMMON_RESULT_ID = 11111L;
    public static final String FAIL_RESULT_ID_STRING = "aaa";

    @Test
    public void isSuccessfulResult() {
        RemoteRequestResult remoteRequestResult = new RemoteRequestResult(SUCCESS_HTTP_CODE, "");
        assertTrue(remoteRequestResult.isSuccessful());
    }

    @Test
    public void isFailResult() {
        RemoteRequestResult remoteRequestResult = new RemoteRequestResult(FAIL_HTTP_CODE, "");
        assertFalse(remoteRequestResult.isSuccessful());
    }

    @Test
    public void getCommonResultId() {
        RemoteRequestResult remoteRequestResult = new RemoteRequestResult(SUCCESS_HTTP_CODE, COMMON_RESULT_ID_STRING);
        assertEquals(remoteRequestResult.getResultId(), COMMON_RESULT_ID);
    }

    @Test(expected=NumberFormatException.class)
    public void getFailResultId() {
        RemoteRequestResult remoteRequestResult = new RemoteRequestResult(SUCCESS_HTTP_CODE, FAIL_RESULT_ID_STRING);
        remoteRequestResult.getResultId();
    }
}
