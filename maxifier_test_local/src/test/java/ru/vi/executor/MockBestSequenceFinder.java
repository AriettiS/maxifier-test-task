package ru.vi.executor;

import ru.vi.model.RequestQueueEntry;

import java.util.Map;
import java.util.Optional;

public class MockBestSequenceFinder implements BestSequenceFinder {
    public static final String BEST_SEQUENCE = "bestSequence";

    @Override
    public Optional<String> find(Map<String, RequestQueueEntry> map) {
        return Optional.of(BEST_SEQUENCE);
    }
}
