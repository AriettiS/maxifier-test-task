package ru.vi.executor;

import ru.vi.http.IdGeneratorServiceHttpClient;
import ru.vi.http.RemoteRequestResult;

public class MockIdGeneratorServiceHttpClient implements IdGeneratorServiceHttpClient {

    public static final int HTTP_OK_CODE = 200;
    public static final long CREATED_IDS_COUNT = 100L;

    @Override
    public RemoteRequestResult getNextId(String sequence, int count) {
        return new RemoteRequestResult(HTTP_OK_CODE, String.valueOf(CREATED_IDS_COUNT));
    }
}
