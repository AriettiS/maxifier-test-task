package ru.vi.executor;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import ru.vi.http.IdGeneratorServiceHttpClient;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static ru.vi.executor.MockBestSequenceFinder.BEST_SEQUENCE;
import static ru.vi.executor.MockIdGeneratorServiceHttpClient.CREATED_IDS_COUNT;

public class RemoteServiceRequestExecutorTest {
    private Injector injector;

    @Before
    public void setUp() throws Exception {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(IdGeneratorServiceHttpClient.class).to(MockIdGeneratorServiceHttpClient.class);
                bind(BestSequenceFinder.class).to(MinDelayBestSequenceFinder.class);
            }
        });
    }

    @Test
    public void getCommonId() throws TimeoutException, InterruptedException {
        RemoteServiceRequestExecutor remoteServiceRequestExecutor = injector.getInstance(RemoteServiceRequestExecutor.class);
        remoteServiceRequestExecutor.execute();
        long id = remoteServiceRequestExecutor.demandId(BEST_SEQUENCE);
        assertEquals(id, CREATED_IDS_COUNT);
    }
}
