package ru.vi.executor;

import org.junit.Test;
import ru.vi.model.RequestQueueEntry;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

public class MinDelayBestSequenceFinderTest {
    public static final String SEQUENCE_MIN = "sequenceMin";
    public static final String SEQUENCE_MAX = "sequenceMax";
    public static final String SEQUENCE_AVG = "sequenceAvg";

    private MinDelayBestSequenceFinder maxWaitingTimeBestSequenceFinder = new MinDelayBestSequenceFinder();

    @Test
    public void findInRequestQueueCase() throws IOException, InterruptedException {
        Map<String, RequestQueueEntry> map = new HashMap<>();

        RequestQueueEntry maxTimeRequestQueue = new RequestQueueEntry();
        addDemandInNewThread(maxTimeRequestQueue);

        Thread.sleep(1000L);

        RequestQueueEntry avgTimeRequestQueue = new RequestQueueEntry();
        addDemandInNewThread(avgTimeRequestQueue);
        addDemandInNewThread(avgTimeRequestQueue);
        addDemandInNewThread(avgTimeRequestQueue);

        Thread.sleep(200L);

        RequestQueueEntry minTimeRequestQueue = new RequestQueueEntry();
        addDemandInNewThread(minTimeRequestQueue);
        addDemandInNewThread(minTimeRequestQueue);
        addDemandInNewThread(minTimeRequestQueue);

        map.put(SEQUENCE_MIN, minTimeRequestQueue);
        map.put(SEQUENCE_MAX, maxTimeRequestQueue);
        map.put(SEQUENCE_AVG, avgTimeRequestQueue);

        Thread.sleep(100L);

        Optional<String> bestSequence = maxWaitingTimeBestSequenceFinder.find(map);
        assertTrue(bestSequence.isPresent());
        assertEquals(bestSequence.get(), SEQUENCE_MAX);
    }

    private void addDemandInNewThread(RequestQueueEntry requestQueueEntry)
    {
        new Thread(() -> {
            try {
                requestQueueEntry.demandId();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }).start();
    }

    @Test
    public void findInEmptyQueue() throws IOException, InterruptedException {
        Map<String, RequestQueueEntry> map = new HashMap<>();
        Optional<String> bestSequence = maxWaitingTimeBestSequenceFinder.find(map);
        assertFalse(bestSequence.isPresent());
    }
}
