package ru.vi;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.log4j.Logger;
import ru.vi.executor.RemoteServiceRequestExecutor;
import ru.vi.modules.AppInjector;
import ru.vi.service.IdGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class StressVolumeTest {
    private final static Logger LOG = Logger.getLogger(StressVolumeTest.class);

    private static final int THREADS_PER_SECOND = 500;
    private static final int SEQUENCES_NUMBER = 100;

    public static void main(String[] args) throws InterruptedException {
        Injector injector = Guice.createInjector(new AppInjector());
        RemoteServiceRequestExecutor remoteServiceRequestExecutor = injector.getInstance(RemoteServiceRequestExecutor.class);
        remoteServiceRequestExecutor.execute();

        IdGenerator idGenerator = injector.getInstance(IdGenerator.class);
        AtomicInteger sequenceIndex = new AtomicInteger(0);

        ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();
        execService.scheduleAtFixedRate(()-> {
            ExecutorService executor = Executors.newCachedThreadPool();
            executor.submit(() -> {
                try {
                    String sequence = "sequence" + (sequenceIndex.getAndIncrement() % SEQUENCES_NUMBER);
                    idGenerator.getNextId(sequence);
                } catch (Exception e) {
                    LOG.error("Client exception: ", e);
                }
            });
        }, 0, 1000 / THREADS_PER_SECOND, TimeUnit.MILLISECONDS);
    }
}
