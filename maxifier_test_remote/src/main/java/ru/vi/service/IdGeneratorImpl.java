package ru.vi.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class IdGeneratorImpl implements IdGenerator {
    private Map<String, Long> nextIdMap = new HashMap<>();
    @Override
    public synchronized long getId(String sequence, int count) {
        Long nextId = nextIdMap.computeIfAbsent(sequence, t -> 1L);
        nextIdMap.put(sequence, nextId + count);
        return nextId;
    }
}

