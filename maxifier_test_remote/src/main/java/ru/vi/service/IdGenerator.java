package ru.vi.service;

public interface IdGenerator {
    long getId(String sequence, int count);
}
