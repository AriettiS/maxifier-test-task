package ru.vi.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vi.service.IdGenerator;

@RestController
@RequestMapping(value = IdGeneratorController.SERVICE_URL_PATH)
public class IdGeneratorController {
    private final static Logger LOG = Logger.getLogger(IdGeneratorController.class);

    static final String SERVICE_URL_PATH = "/getid";
    private static final String GET_ID_PATH = "/{sequence}/{count}";
    private static final String SEQUENCE_PATH_VARIABLE = "sequence";
    private static final String COUNT_PATH_VARIABLE = "count";
    private static final long DELAY_TIME_MS = 1000L;

    @Autowired
    private IdGenerator idGenerator;

    @RequestMapping(value = GET_ID_PATH, method = RequestMethod.GET)
    @ResponseBody
    public long getId(@PathVariable(SEQUENCE_PATH_VARIABLE) String sequence,
                      @PathVariable(COUNT_PATH_VARIABLE) int count) {
        this.delay();
        long id = idGenerator.getId(sequence, count);
        LOG.debug("sequence: " + sequence + "; count: " + count + "; id: " + id);
        return id;
    }

    private void delay()
    {
        try {
            Thread.sleep(DELAY_TIME_MS);
        } catch (InterruptedException e) {
            LOG.error("InterruptedException: ", e);
        }
    }


}